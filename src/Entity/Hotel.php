<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HotelRepository")
 */
class Hotel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ville", inversedBy="hotels")
     */
    private $ville;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $adult_only;

    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Chambrehotel", mappedBy="hotel")
     */
    private $chambres;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VenteHotel", mappedBy="hotelId")
     */
    private $venteHotels;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Saison", mappedBy="id_hotel")
     */
    private $saisons;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ContactHotel", mappedBy="id_hotel")
     */
    private $contactHotels;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\StopSales", mappedBy="id_hotel")
     */
    private $stopSales;

    public function __construct()
    {
        $this->chambres = new ArrayCollection();
        $this->venteHotels = new ArrayCollection();
        $this->saisons = new ArrayCollection();
        $this->contactHotels = new ArrayCollection();
        $this->stopSales = new ArrayCollection();
    }

   
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getAdultOnly(): ?bool
    {
        return $this->adult_only;
    }

    public function setAdultOnly(?bool $adult_only): self
    {
        $this->adult_only = $adult_only;

        return $this;
    }

    
    
    /**
     * @return Collection|Chambrehotel[]
     */
    public function getChambres(): Collection
    {
        return $this->chambres;
    }

    public function addChambre(Chambrehotel $chambre): self
    {
        if (!$this->chambres->contains($chambre)) {
            $this->chambres[] = $chambre;
            $chambre->setHotel($this);
        }

        return $this;
    }

    public function removeChambre(Chambrehotel $chambre): self
    {
        if ($this->chambres->contains($chambre)) {
            $this->chambres->removeElement($chambre);
            // set the owning side to null (unless already changed)
            if ($chambre->getHotel() === $this) {
                $chambre->setHotel(null);
            }
        }

        return $this;
    }

    public function __toString(){

        return $this->nom;
       
    }

    /**
     * @return Collection|VenteHotel[]
     */
    public function getVenteHotels(): Collection
    {
        return $this->venteHotels;
    }

    public function addVenteHotel(VenteHotel $venteHotel): self
    {
        if (!$this->venteHotels->contains($venteHotel)) {
            $this->venteHotels[] = $venteHotel;
            $venteHotel->setHotelId($this);
        }

        return $this;
    }

    public function removeVenteHotel(VenteHotel $venteHotel): self
    {
        if ($this->venteHotels->contains($venteHotel)) {
            $this->venteHotels->removeElement($venteHotel);
            // set the owning side to null (unless already changed)
            if ($venteHotel->getHotelId() === $this) {
                $venteHotel->setHotelId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Saison[]
     */
    public function getSaisons(): Collection
    {
        return $this->saisons;
    }

    public function addSaison(Saison $saison): self
    {
        if (!$this->saisons->contains($saison)) {
            $this->saisons[] = $saison;
            $saison->setIdHotel($this);
        }

        return $this;
    }

    public function removeSaison(Saison $saison): self
    {
        if ($this->saisons->contains($saison)) {
            $this->saisons->removeElement($saison);
            // set the owning side to null (unless already changed)
            if ($saison->getIdHotel() === $this) {
                $saison->setIdHotel(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ContactHotel[]
     */
    public function getContactHotels(): Collection
    {
        return $this->contactHotels;
    }

    public function addContactHotel(ContactHotel $contactHotel): self
    {
        if (!$this->contactHotels->contains($contactHotel)) {
            $this->contactHotels[] = $contactHotel;
            $contactHotel->setIdHotel($this);
        }

        return $this;
    }

    public function removeContactHotel(ContactHotel $contactHotel): self
    {
        if ($this->contactHotels->contains($contactHotel)) {
            $this->contactHotels->removeElement($contactHotel);
            // set the owning side to null (unless already changed)
            if ($contactHotel->getIdHotel() === $this) {
                $contactHotel->setIdHotel(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|StopSales[]
     */
    public function getStopSales(): Collection
    {
        return $this->stopSales;
    }

    public function addStopSale(StopSales $stopSale): self
    {
        if (!$this->stopSales->contains($stopSale)) {
            $this->stopSales[] = $stopSale;
            $stopSale->setIdHotel($this);
        }

        return $this;
    }

    public function removeStopSale(StopSales $stopSale): self
    {
        if ($this->stopSales->contains($stopSale)) {
            $this->stopSales->removeElement($stopSale);
            // set the owning side to null (unless already changed)
            if ($stopSale->getIdHotel() === $this) {
                $stopSale->setIdHotel(null);
            }
        }

        return $this;
    }

}
