<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VenteHotelRepository")
 */
class VenteHotel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="venteHotels")
     */
    private $id_client;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $passager;

    /**
     * @ORM\Column(type="integer")
     */
    private $phone;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Hotel", inversedBy="venteHotels")
     */
    private $hotelId;

    /**
     * @ORM\Column(type="date")
     */
    private $entree;

    /**
     * @ORM\Column(type="date")
     */
    private $sortie;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Arrangement", inversedBy="venteHotels")
     */
    private $arrangement;

    /**
     * @ORM\Column(type="float")
     */
    private $achat;

    /**
     * @ORM\Column(type="float")
     */
    private $vente;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $remarque;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ClientIndiv", inversedBy="venteHotels")
     */
    private $id_client_indiv;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $uniqueId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $idBrave;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Users", inversedBy="venteHotels")
     */
    private $id_user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getIdClient(): ?Client
    {
        return $this->id_client;
    }

    public function setIdClient(?Client $id_client): self
    {
        $this->id_client = $id_client;

        return $this;
    }

    public function getPassager(): ?string
    {
        return $this->passager;
    }

    public function setPassager(string $passager): self
    {
        $this->passager = $passager;

        return $this;
    }

    public function getPhone(): ?int
    {
        return $this->phone;
    }

    public function setPhone(int $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getHotelId(): ?Hotel
    {
        return $this->hotelId;
    }

    public function setHotelId(?Hotel $hotelId): self
    {
        $this->hotelId = $hotelId;

        return $this;
    }

    public function getEntree(): ?\DateTimeInterface
    {
        return $this->entree;
    }

    public function setEntree(\DateTimeInterface $entree): self
    {
        $this->entree = $entree;

        return $this;
    }

    public function getSortie(): ?\DateTimeInterface
    {
        return $this->sortie;
    }

    public function setSortie(\DateTimeInterface $sortie): self
    {
        $this->sortie = $sortie;

        return $this;
    }

    public function getArrangement(): ?Arrangement
    {
        return $this->arrangement;
    }

    public function setArrangement(?Arrangement $arrangement): self
    {
        $this->arrangement = $arrangement;

        return $this;
    }

    public function getAchat(): ?float
    {
        return $this->achat;
    }

    public function setAchat(float $achat): self
    {
        $this->achat = $achat;

        return $this;
    }

    public function getVente(): ?float
    {
        return $this->vente;
    }

    public function setVente(float $vente): self
    {
        $this->vente = $vente;

        return $this;
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(?string $remarque): self
    {
        $this->remarque = $remarque;

        return $this;
    }

    public function getIdClientIndiv(): ?ClientIndiv
    {
        return $this->id_client_indiv;
    }

    public function setIdClientIndiv(?ClientIndiv $id_client_indiv): self
    {
        $this->id_client_indiv = $id_client_indiv;

        return $this;
    }

    public function getUniqueId(): ?string
    {
        return $this->uniqueId;
    }

    public function setUniqueId(string $uniqueId): self
    {
        $this->uniqueId = $uniqueId;

        return $this;
    }

    public function getIdBrave(): ?string
    {
        return $this->idBrave;
    }

    public function setIdBrave(string $idBrave): self
    {
        $this->idBrave = $idBrave;

        return $this;
    }

    public function getIdUser(): ?Users
    {
        return $this->id_user;
    }

    public function setIdUser(?Users $id_user): self
    {
        $this->id_user = $id_user;

        return $this;
    }
}
