<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PriceArrangementRepository")
 */
class PriceArrangement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Arrangement", inversedBy="priceArrangements")
     */
    private $arrangement;

    /**
     * @ORM\Column(type="float")
     */
    private $achat;

    /**
     * @ORM\Column(type="float")
     */
    private $vente;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Saison", inversedBy="priceArrangements")
     */
    private $saison;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArrangement(): ?Arrangement
    {
        return $this->arrangement;
    }

    public function setArrangement(?Arrangement $arrangement): self
    {
        $this->arrangement = $arrangement;

        return $this;
    }

    public function getAchat(): ?float
    {
        return $this->achat;
    }

    public function setAchat(float $achat): self
    {
        $this->achat = $achat;

        return $this;
    }

    public function getVente(): ?float
    {
        return $this->vente;
    }

    public function setVente(float $vente): self
    {
        $this->vente = $vente;

        return $this;
    }

    public function getSaison(): ?Saison
    {
        return $this->saison;
    }

    public function setSaison(?Saison $saison): self
    {
        $this->saison = $saison;

        return $this;
    }
}
