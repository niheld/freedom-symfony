<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArrangementRepository")
 */
class Arrangement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $arrangement;

   
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Chambrehotel", mappedBy="arrangement")
     */
    private $chambres;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VenteHotel", mappedBy="arrangement")
     */
    private $venteHotels;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PriceArrangement", mappedBy="arrangement")
     */
    private $priceArrangements;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    public function __construct()
    {
        $this->chambres = new ArrayCollection();
        $this->venteHotels = new ArrayCollection();
        $this->priceArrangements = new ArrayCollection();
    }

    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArrangement(): ?string
    {
        return $this->arrangement;
    }

    public function setArrangement(?string $arrangement): self
    {
        $this->arrangement = $arrangement;

        return $this;
    }

    
    /**
     * @return Collection|Chambrehotel[]
     */
    public function getChambres(): Collection
    {
        return $this->chambres;
    }

    public function addChambre(Chambrehotel $chambre): self
    {
        if (!$this->chambres->contains($chambre)) {
            $this->chambres[] = $chambre;
            $chambre->setArrangement($this);
        }

        return $this;
    }

    public function removeChambre(Chambrehotel $chambre): self
    {
        if ($this->chambres->contains($chambre)) {
            $this->chambres->removeElement($chambre);
            // set the owning side to null (unless already changed)
            if ($chambre->getArrangement() === $this) {
                $chambre->setArrangement(null);
            }
        }

        return $this;
    }

    public function __toString(){

        return $this->arrangement;
       
    }

    /**
     * @return Collection|VenteHotel[]
     */
    public function getVenteHotels(): Collection
    {
        return $this->venteHotels;
    }

    public function addVenteHotel(VenteHotel $venteHotel): self
    {
        if (!$this->venteHotels->contains($venteHotel)) {
            $this->venteHotels[] = $venteHotel;
            $venteHotel->setArrangement($this);
        }

        return $this;
    }

    public function removeVenteHotel(VenteHotel $venteHotel): self
    {
        if ($this->venteHotels->contains($venteHotel)) {
            $this->venteHotels->removeElement($venteHotel);
            // set the owning side to null (unless already changed)
            if ($venteHotel->getArrangement() === $this) {
                $venteHotel->setArrangement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PriceArrangement[]
     */
    public function getPriceArrangements(): Collection
    {
        return $this->priceArrangements;
    }

    public function addPriceArrangement(PriceArrangement $priceArrangement): self
    {
        if (!$this->priceArrangements->contains($priceArrangement)) {
            $this->priceArrangements[] = $priceArrangement;
            $priceArrangement->setArrangement($this);
        }

        return $this;
    }

    public function removePriceArrangement(PriceArrangement $priceArrangement): self
    {
        if ($this->priceArrangements->contains($priceArrangement)) {
            $this->priceArrangements->removeElement($priceArrangement);
            // set the owning side to null (unless already changed)
            if ($priceArrangement->getArrangement() === $this) {
                $priceArrangement->setArrangement(null);
            }
        }

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }
}
