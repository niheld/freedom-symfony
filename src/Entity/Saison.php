<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SaisonRepository")
 */
class Saison
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $agent;

    /**
     * @ORM\Column(type="date")
     */
    private $date_saisie;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Hotel", inversedBy="saisons")
     */
    private $id_hotel;

    /**
     * @ORM\Column(type="date")
     */
    private $date_debut;

    /**
     * @ORM\Column(type="date")
     */
    private $date_fin;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PriceArrangement", mappedBy="saison")
     */
    private $priceArrangements;

    public function __construct()
    {
        $this->priceArrangements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAgent(): ?int
    {
        return $this->agent;
    }

    public function setAgent(int $agent): self
    {
        $this->agent = $agent;

        return $this;
    }

    public function getDateSaisie(): ?\DateTimeInterface
    {
        return $this->date_saisie;
    }

    public function setDateSaisie(\DateTimeInterface $date_saisie): self
    {
        $this->date_saisie = $date_saisie;

        return $this;
    }

    public function getIdHotel(): ?Hotel
    {
        return $this->id_hotel;
    }

    public function setIdHotel(?Hotel $id_hotel): self
    {
        $this->id_hotel = $id_hotel;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->date_debut;
    }

    public function setDateDebut(\DateTimeInterface $date_debut): self
    {
        $this->date_debut = $date_debut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->date_fin;
    }

    public function setDateFin(\DateTimeInterface $date_fin): self
    {
        $this->date_fin = $date_fin;

        return $this;
    }

    /**
     * @return Collection|PriceArrangement[]
     */
    public function getPriceArrangements(): Collection
    {
        return $this->priceArrangements;
    }

    public function addPriceArrangement(PriceArrangement $priceArrangement): self
    {
        if (!$this->priceArrangements->contains($priceArrangement)) {
            $this->priceArrangements[] = $priceArrangement;
            $priceArrangement->setSaison($this);
        }

        return $this;
    }

    public function removePriceArrangement(PriceArrangement $priceArrangement): self
    {
        if ($this->priceArrangements->contains($priceArrangement)) {
            $this->priceArrangements->removeElement($priceArrangement);
            // set the owning side to null (unless already changed)
            if ($priceArrangement->getSaison() === $this) {
                $priceArrangement->setSaison(null);
            }
        }

        return $this;
    }
}
