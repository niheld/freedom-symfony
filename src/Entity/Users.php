<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsersRepository")
 */
class Users
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom_user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom_user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse_user;

    /**
     * @ORM\Column(type="integer")
     */
    private $tel_user;

    /**
     * @ORM\Column(type="text")
     */
    private $rib_user;

    /**
     * @ORM\Column(type="text")
     */
    private $cnss_user;

    /**
     * @ORM\Column(type="text")
     */
    private $fonction_user;

    /**
     * @ORM\Column(type="text")
     */
    private $s_f_user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $enf_charge_user;

    /**
     * @ORM\Column(type="text")
     */
    private $type_contrat;

    /**
     * @ORM\Column(type="text")
     */
    private $debut_contrat;

    /**
     * @ORM\Column(type="text")
     */
    private $fin_contrat;

    /**
     * @ORM\Column(type="text")
     */
    private $log;

    /**
     * @ORM\Column(type="text")
     */
    private $pass;

    /**
     * @ORM\Column(type="text")
     */
    private $mail;

    /**
     * @ORM\Column(type="text")
     */
    private $tel;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Client", mappedBy="id_user")
     */
    private $clients;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VenteHotel", mappedBy="id_user")
     */
    private $venteHotels;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AutorisationSortie", mappedBy="id_user")
     */
    private $autorisationSorties;

    public function __construct()
    {
        $this->clients = new ArrayCollection();
        $this->venteHotels = new ArrayCollection();
        $this->autorisationSorties = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomUser(): ?string
    {
        return $this->nom_user;
    }

    public function setNomUser(string $nom_user): self
    {
        $this->nom_user = $nom_user;

        return $this;
    }

    public function getPrenomUser(): ?string
    {
        return $this->prenom_user;
    }

    public function setPrenomUser(string $prenom_user): self
    {
        $this->prenom_user = $prenom_user;

        return $this;
    }

    public function getAdresseUser(): ?string
    {
        return $this->adresse_user;
    }

    public function setAdresseUser(string $adresse_user): self
    {
        $this->adresse_user = $adresse_user;

        return $this;
    }

    public function getTelUser(): ?int
    {
        return $this->tel_user;
    }

    public function setTelUser(int $tel_user): self
    {
        $this->tel_user = $tel_user;

        return $this;
    }

    public function getRibUser(): ?string
    {
        return $this->rib_user;
    }

    public function setRibUser(string $rib_user): self
    {
        $this->rib_user = $rib_user;

        return $this;
    }

    public function getCnssUser(): ?string
    {
        return $this->cnss_user;
    }

    public function setCnssUser(string $cnss_user): self
    {
        $this->cnss_user = $cnss_user;

        return $this;
    }

    public function getFonctionUser(): ?string
    {
        return $this->fonction_user;
    }

    public function setFonctionUser(string $fonction_user): self
    {
        $this->fonction_user = $fonction_user;

        return $this;
    }

    public function getSFUser(): ?string
    {
        return $this->s_f_user;
    }

    public function setSFUser(string $s_f_user): self
    {
        $this->s_f_user = $s_f_user;

        return $this;
    }

    public function getEnfChargeUser(): ?string
    {
        return $this->enf_charge_user;
    }

    public function setEnfChargeUser(string $enf_charge_user): self
    {
        $this->enf_charge_user = $enf_charge_user;

        return $this;
    }

    public function getTypeContrat(): ?string
    {
        return $this->type_contrat;
    }

    public function setTypeContrat(string $type_contrat): self
    {
        $this->type_contrat = $type_contrat;

        return $this;
    }

    public function getDebutContrat(): ?string
    {
        return $this->debut_contrat;
    }

    public function setDebutContrat(string $debut_contrat): self
    {
        $this->debut_contrat = $debut_contrat;

        return $this;
    }

    public function getFinContrat(): ?string
    {
        return $this->fin_contrat;
    }

    public function setFinContrat(string $fin_contrat): self
    {
        $this->fin_contrat = $fin_contrat;

        return $this;
    }

    public function getLog(): ?string
    {
        return $this->log;
    }

    public function setLog(string $log): self
    {
        $this->log = $log;

        return $this;
    }

    public function getPass(): ?string
    {
        return $this->pass;
    }

    public function setPass(string $pass): self
    {
        $this->pass = $pass;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * @return Collection|Client[]
     */
    public function getClients(): Collection
    {
        return $this->clients;
    }

    public function addClient(Client $client): self
    {
        if (!$this->clients->contains($client)) {
            $this->clients[] = $client;
            $client->setIdUser($this);
        }

        return $this;
    }

    public function removeClient(Client $client): self
    {
        if ($this->clients->contains($client)) {
            $this->clients->removeElement($client);
            // set the owning side to null (unless already changed)
            if ($client->getIdUser() === $this) {
                $client->setIdUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|VenteHotel[]
     */
    public function getVenteHotels(): Collection
    {
        return $this->venteHotels;
    }

    public function addVenteHotel(VenteHotel $venteHotel): self
    {
        if (!$this->venteHotels->contains($venteHotel)) {
            $this->venteHotels[] = $venteHotel;
            $venteHotel->setIdUser($this);
        }

        return $this;
    }

    public function removeVenteHotel(VenteHotel $venteHotel): self
    {
        if ($this->venteHotels->contains($venteHotel)) {
            $this->venteHotels->removeElement($venteHotel);
            // set the owning side to null (unless already changed)
            if ($venteHotel->getIdUser() === $this) {
                $venteHotel->setIdUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AutorisationSortie[]
     */
    public function getAutorisationSorties(): Collection
    {
        return $this->autorisationSorties;
    }

    public function addAutorisationSorty(AutorisationSortie $autorisationSorty): self
    {
        if (!$this->autorisationSorties->contains($autorisationSorty)) {
            $this->autorisationSorties[] = $autorisationSorty;
            $autorisationSorty->setIdUser($this);
        }

        return $this;
    }

    public function removeAutorisationSorty(AutorisationSortie $autorisationSorty): self
    {
        if ($this->autorisationSorties->contains($autorisationSorty)) {
            $this->autorisationSorties->removeElement($autorisationSorty);
            // set the owning side to null (unless already changed)
            if ($autorisationSorty->getIdUser() === $this) {
                $autorisationSorty->setIdUser(null);
            }
        }

        return $this;
    }
}
