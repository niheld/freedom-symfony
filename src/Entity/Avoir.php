<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AvoirRepository")
 */
class Avoir
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="text")
     */
    private $fournisseur;

    /**
     * @ORM\Column(type="text")
     */
    private $reservation;

    /**
     * @ORM\Column(type="float")
     */
    private $achat;

    /**
     * @ORM\Column(type="float")
     */
    private $vente;

    /**
     * @ORM\Column(type="float")
     */
    private $avoir;

    /**
     * @ORM\Column(type="text")
     */
    private $etat;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getFournisseur(): ?string
    {
        return $this->fournisseur;
    }

    public function setFournisseur(string $fournisseur): self
    {
        $this->fournisseur = $fournisseur;

        return $this;
    }

    public function getReservation(): ?string
    {
        return $this->reservation;
    }

    public function setReservation(string $reservation): self
    {
        $this->reservation = $reservation;

        return $this;
    }

    public function getAchat(): ?float
    {
        return $this->achat;
    }

    public function setAchat(float $achat): self
    {
        $this->achat = $achat;

        return $this;
    }

    public function getVente(): ?float
    {
        return $this->vente;
    }

    public function setVente(float $vente): self
    {
        $this->vente = $vente;

        return $this;
    }

    public function getAvoir(): ?float
    {
        return $this->avoir;
    }

    public function setAvoir(float $avoir): self
    {
        $this->avoir = $avoir;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }
}
