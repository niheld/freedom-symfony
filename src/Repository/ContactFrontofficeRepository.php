<?php

namespace App\Repository;

use App\Entity\ContactFrontoffice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ContactFrontoffice|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactFrontoffice|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactFrontoffice[]    findAll()
 * @method ContactFrontoffice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactFrontofficeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContactFrontoffice::class);
    }

    // /**
    //  * @return ContactFrontoffice[] Returns an array of ContactFrontoffice objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ContactFrontoffice
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
