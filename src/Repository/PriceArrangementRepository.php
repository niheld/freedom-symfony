<?php

namespace App\Repository;

use App\Entity\PriceArrangement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PriceArrangement|null find($id, $lockMode = null, $lockVersion = null)
 * @method PriceArrangement|null findOneBy(array $criteria, array $orderBy = null)
 * @method PriceArrangement[]    findAll()
 * @method PriceArrangement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PriceArrangementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PriceArrangement::class);
    }

    // /**
    //  * @return PriceArrangement[] Returns an array of PriceArrangement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PriceArrangement
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
