<?php

namespace App\Repository;

use App\Entity\ContactHotel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ContactHotel|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactHotel|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactHotel[]    findAll()
 * @method ContactHotel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactHotelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContactHotel::class);
    }

    // /**
    //  * @return ContactHotel[] Returns an array of ContactHotel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ContactHotel
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
