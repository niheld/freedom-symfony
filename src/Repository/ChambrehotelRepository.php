<?php

namespace App\Repository;

use App\Entity\Chambrehotel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Chambrehotel|null find($id, $lockMode = null, $lockVersion = null)
 * @method Chambrehotel|null findOneBy(array $criteria, array $orderBy = null)
 * @method Chambrehotel[]    findAll()
 * @method Chambrehotel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChambrehotelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Chambrehotel::class);
    }

    // /**
    //  * @return Chambrehotel[] Returns an array of Chambrehotel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Chambrehotel
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
