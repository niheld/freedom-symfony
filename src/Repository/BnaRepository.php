<?php

namespace App\Repository;

use App\Entity\Bna;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Bna|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bna|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bna[]    findAll()
 * @method Bna[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BnaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Bna::class);
    }

    // /**
    //  * @return Bna[] Returns an array of Bna objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Bna
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
